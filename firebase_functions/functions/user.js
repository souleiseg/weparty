const admin = require("firebase-admin");
const {getDocumentId, deleteDocument, getter, createOrUpdate} = require("./crud");

const usersCollection = admin.firestore().collection("users");
const userRoute = "user";

const createOrUpdateUser = (userId, body, response) => {
  createOrUpdate(usersCollection, userId, body, response, userRoute, () => {
    getUser(userId, response);
  });
};

const updateUser = (userId, body, response) => {
  getter(usersCollection, userId, response, userRoute, (snapshotData) => {
    if (!snapshotData) return response.send("User doesn't exists, you can't edit it");
    createOrUpdateUser(userId, body, response);
  });
};

const createUser = (userId, body, response) => {
  getter(usersCollection, userId, response, userRoute, (snapshotData) => {
    if (snapshotData) return response.send("User already exists, you can't override it");
    createOrUpdateUser(userId, body, response);
  });
};

const getUser = (userId, response) => {
  getter(usersCollection, userId, response, userRoute, (snapshotData) => {
    if (!snapshotData) return response.send("User doesn't exists");
    return response.send(snapshotData);
  });
};

const user = (request, response) => {
  const userId = getDocumentId(request.url);
  if (!userId) return response.send("Please use a userId");

  switch (request.method) {
    case "GET":
      getUser(userId, response);
      break;
    case "POST":
      createUser(userId, request.body, response);
      break;
    case "PATCH":
      updateUser(userId, request.body, response);
      break;
    case "DELETE":
      deleteDocument(usersCollection, userId, response, userRoute, "User");
      break;
    default:
      response.send("Unsupported method");
  }
};

module.exports = user;

const admin = require("firebase-admin");
const {getDocumentId, deleteDocument, getter, createOrUpdate} = require("./crud");

const partiesCollection = admin.firestore().collection("parties");
const partyRoute = "party";

const createOrUpdateParty = (partyId, body, response) => {
  createOrUpdate(partiesCollection, partyId, body, response, partyRoute, () => {
    getParty(partyId, response);
  });
};

const updateParty = (partyId, body, response) => {
  getter(partiesCollection, partyId, response, partyRoute, (snapshotData) => {
    if (!snapshotData) return response.send("Party doesn't exists, you can't edit it");
    createOrUpdateParty(partyId, body, response);
  });
};

const createParty = (body, response) => {
  const partyId = partiesCollection.doc().id;
  body["partyId"] = partyId;
  createOrUpdateParty(partyId, body, response);
};

const getParty = (partyId, response) => {
  getter(partiesCollection, partyId, response, partyRoute, (snapshotData) => {
    if (!snapshotData) return response.send("Party doesn't exists");
    return response.send(snapshotData);
  });
};

const party = (request, response) => {
  const partyId = getDocumentId(request.url);

  switch (request.method) {
    case "GET":
      if (!partyId) return response.send("Please use a partyId");
      getParty(partyId, response);
      break;
    case "POST":
      createParty(request.body, response);
      break;
    case "PATCH":
      if (!partyId) return response.send("Please use a partyId");
      updateParty(partyId, request.body, response);
      break;
    case "DELETE":
      if (!partyId) return response.send("Please use a partyId");
      deleteDocument(partiesCollection, partyId, response, partyRoute, "Party");
      break;
    default:
      response.send("Unsupported method");
  }
};

module.exports = party;

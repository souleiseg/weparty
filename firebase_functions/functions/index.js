const functions = require("firebase-functions");
const admin = require("firebase-admin");

admin.initializeApp();
const user = require("./user");
const party = require("./party");

exports.users = functions
    .region("europe-west1")
    .https.onRequest((request, response) => {
      try {
        const username = request.query.username;
        const ids = request.query.ids;
        admin
            .firestore()
            .collection("users")
            .get()
            .then((snapshot) => {
              const result = [];
              snapshot.forEach((doc) => {
                const data = doc.data();

                const conditions = [];
                if (username) {
                  conditions.push(data.username.toLowerCase().includes(username.toLowerCase()));
                }
                if (ids) {
                  conditions.push(ids.includes(data.userId));
                }

                if (conditions.length !== 0 && !conditions.includes(false)) {
                  result.push(data);
                }

                // Par défaut on renvoie tout
                if (!username && !ids) {
                  result.push(data);
                }
              });
              response.send(result);
            });
      } catch (err) {
        functions.logger.error("/users failed", err);
        response.send(err.message);
      }
    });

exports.parties = functions
    .region("europe-west1")
    .https.onRequest((request, response) => {
      try {
        const userId = request.query.userId;
        const date = request.query.date;
        admin
            .firestore()
            .collection("parties")
            .get()
            .then((snapshot) => {
              const result = [];
              snapshot.forEach((doc) => {
                const data = doc.data();

                const conditions = [];
                if (userId) {
                  conditions.push(data.guests.some((guest) => guest.userId === userId));
                }
                if (date) {
                  conditions.push((new Date(data.date)).getTime() > (new Date(date)).getTime());
                }

                if (conditions.length !== 0 && !conditions.includes(false)) {
                  result.push(data);
                }

                if (!userId && !date) {
                  result.push(data);
                }
              });
              response.send(result);
            });
      } catch (err) {
        functions.logger.error("/parties failed", err);
        response.send(err.message);
      }
    });

exports.user = functions
    .region("europe-west1")
    .https.onRequest(user);

exports.party = functions
    .region("europe-west1")
    .https.onRequest(party);

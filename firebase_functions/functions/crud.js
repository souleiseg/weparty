const functions = require("firebase-functions");
const url = require("url");

const printError = (route, response, err) => {
  functions.logger.error(`/${route} failed`, err);
  response.send(err.message);
};

const createOrUpdate = (collection, documentId, body, response, route, treatment) => {
  collection
      .doc(documentId)
      .set(body, {merge: true})
      .then(() => treatment(documentId, response))
      .catch((err) => printError(route, response, err));
};

const getter = (collection, documentId, response, route, treatment) => {
  collection
      .doc(documentId)
      .get()
      .then((snapshot) => {
        treatment(snapshot.data());
      })
      .catch((err) => printError(route, response, err));
};

const deleteDocument = (collection, documentId, response, route, collectionName) => {
  collection
      .doc(documentId)
      .delete()
      .then(() => response.send(`${collectionName} successfully deleted`))
      .catch((err) => printError(route, response, err));
};

const getDocumentId = (urlRequest) => {
  return url.parse(urlRequest, true).pathname.slice(1);
};

module.exports = {
  createOrUpdate,
  getter,
  deleteDocument,
  getDocumentId,
};

import React from 'react';
import * as eva from '@eva-design/eva';
import {ApplicationProvider, IconRegistry} from "@ui-kitten/components";
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import AppNavigator from "./src/components/Navigation";
import { default as mapping } from './eva-custom-mapping.json'

export default () => {
    return (
        <>
            <IconRegistry icons={EvaIconsPack} />
            <ApplicationProvider
                {...eva}
                customMapping={mapping}
                theme={eva.dark}
            >
                <AppNavigator />
            </ApplicationProvider>
        </>
    );
}

import { Alert } from "react-native";

const getData = async (url) => {
    try {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    } catch (err) {
        Alert.alert(
            "Erreur lors de la récupération des données",
            err.message
        );
    }
};

const sendData = async (url, method, data) => {
    try {
        const response = await fetch(url, {
            method: method,
            body: JSON.stringify(data),
            headers: { "Content-Type": "application/json" },
        });
        const result = await response.json();
        return result;
    } catch (err) {
        Alert.alert(
            "Erreur lors de la mise à jour des données",
            err.message
        );
    }
};

export const getUsers = async (param, value) =>
    getData(
        `https://europe-west1-weparty-2f373.cloudfunctions.net/users?${param}=${value}`
    );

export const getUser = async (userId) =>
    getData(
        `https://europe-west1-weparty-2f373.cloudfunctions.net/user/` + userId
    );

export const updateUser = async (userId, user) =>
    sendData(
        `https://europe-west1-weparty-2f373.cloudfunctions.net/user/` + userId,
        "PATCH",
        user
    );

export const deleteUser = async (userId) =>
    sendData(
        `https://europe-west1-weparty-2f373.cloudfunctions.net/user/` + userId,
        "DELETE"
    );

export const postUser = async (userId, user) =>
    sendData(
        `https://europe-west1-weparty-2f373.cloudfunctions.net/user/` + userId,
        "POST",
        user
    );

export const getParties = async (userId, date) =>
    getData(
      `https://europe-west1-weparty-2f373.cloudfunctions.net/parties?userId=${userId}&date=${encodeURIComponent(date)}`
    );


export const getParty = async (partyId) => {
    let party = await getData(
      `https://europe-west1-weparty-2f373.cloudfunctions.net/party/` + partyId
    );

    const ids = party.guests.map(guest => guest.userId)
    let users = await getUsers("ids", encodeURIComponent(ids))
    for (let guest of party.guests) {
        for (let user of users) {
            if (guest.userId === user.userId) {
                guest.username = user.username;
                break;
            }
        }
    }
    return party;
}

export const updateParty = async (partyId, party) =>
    sendData(
        `https://europe-west1-weparty-2f373.cloudfunctions.net/party/` + partyId,
        "PATCH",
        party
    );

export const deleteParty = async (partyId) =>
    sendData(
        `https://europe-west1-weparty-2f373.cloudfunctions.net/party/` + partyId,
        "DELETE"
    );

export const postParty = async (party) =>
    sendData(
        `https://europe-west1-weparty-2f373.cloudfunctions.net/party/`,
        "POST",
        party
    );
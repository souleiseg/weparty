import React, {useState} from 'react';
import {Button, Layout, List, ListItem, Icon, Text} from '@ui-kitten/components';
import {useFocusEffect} from "@react-navigation/native";
import { getUser, getParties } from '../api';
import {getCurrentUserId} from "../firebase";

const Home = ({ navigation }) => {


    const [data, setData] = useState([])

    const focusEffect = async () => {
        getCurrentUserId()
        .then(userId => {
            getParties(userId, new Date()).then(parties => {
                for (let party of parties){
                    party.date = new Date(party.date)
                }
                setData(parties)
            })
        })
    }

    useFocusEffect(
        React.useCallback(() => {
          focusEffect()
        }, [])
    );

    const descriptionDate = (date) => {
        return `${date?.toLocaleDateString('fr-FR')} ${date?.getHours()}h${date?.getMinutes()}`
    }

    const renderItemIcon = (props) => (
        <Icon {...props} name='external-link-outline'/>
    );
    
    const PartyList = data.map(function(item, key){
        return (
            <ListItem
                title={item.name}
                description={descriptionDate(item.date)}
                accessoryLeft={renderItemIcon}
                key={key}
                onPress={() => navigateToDetailParty(item.partyId)}
            />
        )
    })

    const navigateToDetailParty = (partyId) => {
        navigation.navigate('Voir une soirée', {partyId})
    }

    return (
        <Layout style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text category="h1" style={{fontSize: 28, marginBottom: '5%'}}>Mes prochaines soirées</Text>
            <Layout style={{margin: '5%', height: 'auto', justifyContent: 'center', alignItems: 'center'}}>
                {PartyList}
            </Layout>
        </Layout>
    );
}

export default Home
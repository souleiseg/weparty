import React, {useState, useContext} from 'react';
import {useFocusEffect} from "@react-navigation/native";
import {Button, Card, Icon, Modal, Input, Divider, Layout, List, ListItem, Text} from '@ui-kitten/components';
import styles from "../styles/PartyDetailsStyles"
import { ListMembers } from "../components/ListMembers"
import { getParty } from "../api";

const PartyDetails = ({ navigation, route, onChildPress }) => {
    

    const navigateToEditParty = () => {
        navigation.navigate('Nouvelle soirée', {partyId: route.params.partyId})
        setVisibleSettings(false);
    }

    const SettingsIcon = (props) => (
      <Icon {...props} name='settings-2-outline'/>
    );

    const deleteIcon = (props) => (
        <Icon {...props} name='close-circle-outline' />
    );

    const shopIcon = (props) => (
        <Icon {...props} name='shopping-bag-outline' />
    );

    const newsIcon = (props) => (
      <Icon {...props} name='alert-circle-outline' />
    );

    const renderIconEdit = () => (
        <Button status="danger" accessoryLeft={deleteIcon} />
    );

    const renderPersonIcon = (props) => (
        <Icon {...props} name='person'/>
    );

    const [visibleMember, setVisibleMember] = useState(false);
    const [visibleMemberEdit, setVisibleMemberEdit] = useState(false);
    const [visibleShop, setVisibleShop] = useState(false);
    const [visibleMessage, setVisibleMessage] = useState(false);
    const [visibleSettings, setVisibleSettings] = useState(false);
    const [owner, setOwner] = useState(true)
    const [data, setData] = useState([]);
    const [courses, setCourses] = useState([])
    const [messages, setMessages] = useState([])

    const focusEffect = async () => {
        if(route.params.partyId){
            getParty(route.params.partyId).then(results => {
                setData({...results, date: new Date(results.date)})
                let tempCourses = [];
                let tempMessages = [];
                for(let guest of results.guests){
                    for(let course of guest.courses){
                        tempCourses.push(course)
                    }
                    for (let message of guest.messages){
                        tempMessages.push(message)
                    }
                }
                setCourses(tempCourses)
                setMessages(tempMessages)
            })
        }
    };

    useFocusEffect(
        React.useCallback(() => {
          focusEffect()
        }, [route.params.partyId])
    );

    const renderCourses = ({ item }) => (
        <ListItem
            title={item}
            style={styles.ModalList}
        />
    );

    const renderMessages = ({ item }) => (
        <ListItem
            title={item}
            style={styles.ModalList}
        />
    );

    const renderMembersEdit = ({ item, index }) => (
        <ListItem
            title={`${item.title} ${index + 1}`}
            accessoryLeft={renderPersonIcon}
            accessoryRight={() => renderIconEdit()}
        />
    );

    return (
        <>
            <Layout style={styles.container} level='1'>
                <Text category='h1' style={{fontSize: 28}}>{data.name}</Text>
                <Card disabled={true} style={{ marginTop: '5%' }}>
                    <Layout style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text category='p2'>{data.date?.toLocaleDateString('fr-FR')}</Text>
                        <Text category='p2'>{data.date?.getHours()}h{data.date?.getMinutes()}</Text>
                    </Layout>
                </Card>
                <Layout style={styles.wrapper}>
                    <Icon
                        style={styles.icon}
                        fill='#8F9BB3'
                        name='map-outline'
                    />
                    <Text style={{ marginLeft: '5%' }} category='p1'>{data.place?.address}</Text>
                </Layout>
                <Layout style={styles.wrapper}>
                    <Icon
                        style={styles.icon}
                        fill='#8F9BB3'
                        name='message-circle-outline'
                    />
                    <Text style={{ marginLeft: '5%' }} category='p1'>{data.description}</Text>
                </Layout>
                <Layout style={[styles.wrapper, styles.wrapperBottom]}>
                    <Button accessoryLeft={shopIcon} status='info' onPress={() => setVisibleShop(true)}>Courses</Button>
                    <Button accessoryLeft={newsIcon} status='info' appearance='outline' onPress={() => setVisibleMessage(true)}>Messages</Button>
                    <Modal 
                        visible={visibleShop} 
                        style={styles.ModalContent}
                        backdropStyle={{backgroundColor: 'rgba(0, 0, 0, 0.5)'}}
                    >
                        <Card disabled={true} style={styles.ModalCard}>
                            <List
                                data={courses}
                                ItemSeparatorComponent={Divider}
                                renderItem={renderCourses}
                            />
                            <Button style={styles.ModalButton} size='small' onPress={() => setVisibleShop(false)}>
                                Fermer
                            </Button>
                        </Card>
                    </Modal>
                    <Modal 
                        visible={visibleMessage} 
                        style={styles.ModalContent}
                        backdropStyle={{backgroundColor: 'rgba(0, 0, 0, 0.5)'}}
                    >
                        <Card disabled={true} style={styles.ModalCard}>
                            <List
                                data={messages}
                                ItemSeparatorComponent={Divider}
                                renderItem={renderMessages}
                            />
                            <Button style={styles.ModalButton} size='small' onPress={() => setVisibleMessage(false)}>
                                Fermer
                            </Button>
                        </Card>
                    </Modal>
                </Layout>
            </Layout>
            <Layout style={styles.containerMedium} level='3'>
                <Text category='h3' style={{ fontSize: 28, marginBottom: 10 }}>Liste des participants</Text>
                <Button size="medium" onPress={() => setVisibleMember(true)}>
                    Voir tous les participants
                </Button>
                <Modal 
                    backdropStyle={{backgroundColor: 'rgba(0, 0, 0, 0.5)'}}
                    visible={visibleMember} 
                    style={styles.ModalContent}
                >
                    <ListMembers onChildPress={() => setVisibleMember(false)} data={data.guests} />
                </Modal>
            </Layout>
            <Layout style={[styles.containerBottom, owner ? styles.isVisible : styles.isHidden ]} level='3'>
                <Button size="medium" status="basic" accessoryLeft={SettingsIcon} onPress={() => setVisibleSettings(true)}>
                </Button>
                <Text category='label'>Paramètres</Text>
                <Modal visible={visibleSettings} style={{ width: '100%', height: '100%', justifyContent: 'center' }}>
                    <Card disabled={true} style={{ flexDirection: "column", justifyContent: "center", height: '100%' }}>
                        <Button size='medium' status='warning' onPress={navigateToEditParty}>
                          Modifier la soirée
                        </Button>
                        <Button size='medium' style={{ marginVertical: "5%" }} status='danger' onPress={() => setVisibleSettings(false)}>
                          Supprimer la soirée
                        </Button>
                        <Button size='medium' onPress={() => setVisibleSettings(false)}>
                            Fermer
                        </Button>
                    </Card>
                </Modal>
            </Layout>
        </>
    )
}

export default PartyDetails
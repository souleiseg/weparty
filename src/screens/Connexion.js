import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import { Layout, Button, Icon, Input } from '@ui-kitten/components';
import loginStyles from "../styles/LoginStyles"
import {redirectToConnexionIfNotAuthenticated, redirectToHomeIfAlreadyAuthenticated, signUserIn} from "../firebase";
import {useFocusEffect} from "@react-navigation/native";

const Connexion = ( {navigation} ) => {
    
    const [User, setUser] = React.useState('');
    const [Password, setPassword] = React.useState('');

    const [secureTextEntry, setSecureTextEntry] = React.useState(true);

    useFocusEffect(
      React.useCallback(() => {
          redirectToHomeIfAlreadyAuthenticated(navigation)
      }, [])
    );

    const toggleSecureEntry = () => {
        setSecureTextEntry(!secureTextEntry);
    };

    const AlertIcon = (props) => (
        <Icon {...props} name='alert-circle-outline'/>
    );

    const renderIcon = (props) => (
        <TouchableWithoutFeedback onPress={toggleSecureEntry}>
            <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'}/>
        </TouchableWithoutFeedback>
    );

    const navigateToInscription =  () => {
        navigation.navigate('Inscription')
    }

    const navigateToHome =  () => {
        navigation.navigate('Home')
    }

    const login = (User, Password) => {
        signUserIn(User, Password).then(navigateToHome)
    }

    return (
        <Layout style={loginStyles.container}>
            <Layout style={loginStyles.layout}>
                <Layout style={loginStyles.formulaire}>
                    <Input
                        style={loginStyles.input}
                        size="large"
                        label="Utilisateur"
                        placeholder="Votre nom d'utilisateur"
                        value={User}
                        onChangeText={nextValue => setUser(nextValue)}
                    />
                    <Input
                        style={loginStyles.input}
                        size="large"
                        value={Password}
                        label='Mot de passe'
                        placeholder="Votre mot de passe"
                        accessoryRight={renderIcon}
                        secureTextEntry={secureTextEntry}
                        onChangeText={nextValue => setPassword(nextValue)}
                    />
                </Layout>
                <Layout level="4" style={loginStyles.bottomLayout}>
                    <Button style={loginStyles.button} status="success" onPress={() => login(User, Password)}>Connexion</Button>
                    <Button style={loginStyles.button} status="primary" onPress={navigateToInscription}>Créer un nouveau compte</Button>
                </Layout>
            </Layout>
        </Layout>
    )
};

export default Connexion
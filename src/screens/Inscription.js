import React from 'react';
import { TouchableWithoutFeedback, View } from 'react-native';
import { Layout, Button, Icon, Text, Input, Toggle } from '@ui-kitten/components';
import loginStyles from "../styles/LoginStyles"
import {signUserIn, signUserUp} from '../firebase'

const Inscription = ( {navigation} ) => {

    const AlertIcon = (props) => (
        <Icon {...props} name='alert-circle-outline'/>
    );
    
    const [secureTextEntry, setSecureTextEntry] = React.useState(true);
    const toggleSecureEntry = () => {
        setSecureTextEntry(!secureTextEntry);
    };

    const renderIcon = (props) => (
        <TouchableWithoutFeedback onPress={toggleSecureEntry}>
            <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'}/>
        </TouchableWithoutFeedback>
    );

    const navigateToConnexion =  () => {
        navigation.navigate('Connexion')
    }

    const navigateToHome =  () => {
        navigation.navigate('Home')
    }

    const signUp = (Email, User, Password) => {
        signUserUp(Email, User, Password).then((isSuccessFull) => {
            if(isSuccessFull){
                navigateToHome()
            }
        })
    }

    /*/ Gestion Formulaire /*/
    const [User, setUser] = React.useState('');
    const [Email, setEmail] = React.useState('');
    const [Password, setPassword] = React.useState('');
    const [secondPassword, setSecondPassword] = React.useState('');

    // Vérifie la longueur du mot de passe
    const [confirmPassword, setConfirmPassword] = React.useState(false);
    const renderPasswordLength = () => {
        React.useEffect(() => {
            if (Password.length > 8) {
                setConfirmPassword(false);
            } else {
                setConfirmPassword(true);
            }
        },[]);
        return (
            <View style={loginStyles.captionContainer}>
                {AlertIcon(loginStyles.captionIcon)}
                <Text style={loginStyles.captionText}>{confirmPassword ? 'Le mot de passe doit contenir au moins 8 caractères' : 'Mot de passe correct'}</Text>
            </View>
        )
    }

    // Vérifie si les 2 mots de passe sont identiques
    const [confirmCaption, setConfirmCaption] = React.useState(false);
    const renderCaption = () => {
        React.useEffect(() => {
            if (Password !== secondPassword ) {
                setConfirmCaption(true);
            } else {
                setConfirmCaption(false);
            }
        },[]);
        return (
        <View style={loginStyles.captionContainer}>
            {AlertIcon(loginStyles.captionIcon)}
            <Text style={loginStyles.captionText}>{confirmCaption ? 'Les mots de passe sont différents' : null}</Text>
        </View>
        )
    }

    const [checked, setChecked] = React.useState(false);

    const onCheckedChange = (isChecked) => {
        setChecked(isChecked);
    };

    return (
        <Layout style={loginStyles.container}>
            <Layout style={loginStyles.layout}>
                <Layout style={loginStyles.formulaire}>
                    <Input
                        style={loginStyles.input}
                        size="large"
                        label="Utilisateur"
                        placeholder="Votre nom d'utilisateur"
                        value={User}
                        onChangeText={nextValue => setUser(nextValue)}
                    />
                    <Input
                        style={loginStyles.input}
                        size="large"
                        label="Adresse mail"
                        placeholder="Votre adresse mail"
                        value={Email}
                        onChangeText={nextValue => setEmail(nextValue)}
                    />
                    <Input
                        style={loginStyles.input}
                        size="large"
                        value={Password}
                        label='Mot de passe'
                        placeholder="Votre mot de passe"
                        caption={renderPasswordLength}
                        accessoryRight={renderIcon}
                        secureTextEntry={secureTextEntry}
                        onChangeText={nextValue => setPassword(nextValue)}
                    />
                    <Input
                        style={loginStyles.input}
                        size="large"
                        value={secondPassword}
                        placeholder="Votre mot de passe"
                        accessoryRight={renderIcon}
                        caption={renderCaption}
                        secureTextEntry={secureTextEntry}
                        onChangeText={nextValue => setSecondPassword(nextValue)}
                    />
                     <Toggle status={checked ? 'info' : 'warning'} checked={checked} onChange={onCheckedChange}>
                        J'accepte les conditions d'utilisation
                    </Toggle>
                    
                </Layout>
                <Layout level="4" style={loginStyles.bottomLayout}>
                    <Button style={loginStyles.button} status="success" disabled={!checked} onPress={() => signUp(Email, User, Password)}>S'inscrire</Button>
                    <Button style={loginStyles.button} status="primary" onPress={navigateToConnexion}>J'ai déjà un compte</Button>
                </Layout>
            </Layout>
        </Layout>
    )
};

export default Inscription

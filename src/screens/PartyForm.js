import React, {useState} from 'react';
import {Button, Datepicker, Icon, Input, Layout, List, ListItem, Text} from '@ui-kitten/components';
import {Timepicker} from "../components/TimePicker";
import {AutocompleteLocation} from "../components/AutocompleteLocation";
import {View} from "react-native";
import {InviteGuests} from "../components/InviteGuests";
import {useFocusEffect} from "@react-navigation/native";
import {getParty, postParty, updateParty} from "../api";

const PartyForm = ({ navigation, route }) => {

    const now = () => {
      let date = new Date();
      const offset = 1;
      date.setHours(date.getHours() + offset)
      return date
    }

    const cleanParty = {
        partyId: NaN,
        name: '',
        description: '',
        place: {
          address: '',
          coordinates: {
            _latitude: NaN,
            _longitude: NaN
          }
        },
        date: now(),
        guests: [],
    }
    const [party, setParty] = useState(cleanParty);
    let placeKey = 0;

    const focusEffect = async () => {
      placeKey+=1
      if (route.params.partyId) {
        navigation.setOptions({title: 'Modification de soirée'})
        getParty(route.params.partyId).then(party => {
          setParty({...party, date: new Date(party.date)})
        })
      } else {
        navigation.setOptions({title: 'Création de soirée'})
        cleanParty.date = now();
        setParty(cleanParty);
      }
    }

    useFocusEffect(
        React.useCallback(() => {
          focusEffect()
        }, [route.params.partyId])
    );

    const deleteGuest = (user) => {
        let tmpGuests = party.guests
        const index = tmpGuests.indexOf(user)
        tmpGuests.splice(index, 1)
        setParty({...party, guests: tmpGuests})
    }

    const cancel = () => {
        navigation.goBack()
    }

    const validate = async () => {
        // To make data consistent with firestore
        for (let guest of party.guests) {
          delete guest.username;
        }

        if (route.params.partyId) {
          await updateParty(route.params.partyId, party)
        } else {
          await postParty(party)
        }
        cancel()
    }

    const renderDeleteGuestAction = (props, user) => (
        <Button size='small' accessoryLeft={renderTrashIcon} onPress={() => deleteGuest(user)}/>
    );

    const renderPersonIcon = (props) => (
        <Icon {...props} name='person'/>
    );

    const renderTrashIcon = (props) => (
        <Icon {...props} name='trash'/>
    );

    const renderGuest = ({item, index}) => (
        <ListItem
            title={item.username}
            description={item.status}
            accessoryLeft={renderPersonIcon}
            accessoryRight={(props) => renderDeleteGuestAction(props, item)}
        />
    );

    return (
        <Layout style={{height: '100%', justifyContent: 'center', alignItems: 'center', padding: 15}}>
            <Input
                label='Nom'
                value={party.name}
                onChangeText={nextValue => setParty({...party, name: nextValue})}
            />
            <Input
                label='Description'
                multiline={true}
                value={party.description}
                onChangeText={nextValue => setParty({...party, description: nextValue})}
            />
            <View style={{width: '100%', flexDirection: 'row'}}>
                <Datepicker
                    style={{width: '70%'}}
                    label='Date'
                    min={now()}
                    date={party.date}
                    onSelect={nextValue => {
                      let tmpValue = party.date;
                      tmpValue.setDate(nextValue.getDate());
                      tmpValue.setMonth(nextValue.getMonth());
                      tmpValue.setFullYear(nextValue.getFullYear());
                      setParty({...party, date: tmpValue})
                    }}
                />
                <Timepicker
                    style={{width: '30%'}}
                    label='Heure'
                    date={party.date}
                    onSelect={nextValue => setParty({...party, date: nextValue})}
                />
            </View>
            <AutocompleteLocation
              key={placeKey}
                label='Lieu'
                defaultValue={party.place.address}
                onSelect={nextValue => setParty({...party, place: nextValue})}
            />

            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', margin: 10}}>
                <Text category='h5'>Liste des invités</Text>
                <InviteGuests
                    label='Trouver un invité'
                    existingGuests={party.guests}
                    onChange={nextValue => setParty({...party, guests: nextValue})}
                />
            </View>

            <List style={{ maxHeight: 200, width: '100%' }} data={party.guests} renderItem={renderGuest}/>

            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 10}}>
                <Button status='danger' onPress={cancel}>Annuler</Button>
                <Button status='success' onPress={validate}>Valider</Button>
            </View>
        </Layout>
    );
}

export default PartyForm
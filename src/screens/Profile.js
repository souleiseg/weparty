import React, { useState, useEffect } from 'react';
import { Layout, Button, Card, Icon, Text } from '@ui-kitten/components';
import {signUserOut, signUserIn, getCurrentUserId} from "../firebase";
import { getUser } from '../api';
import { Avatar } from '@ui-kitten/components';

const Profile = ( {navigation} ) => {
  const navigateToConnexion =  () => {
    navigation.navigate('Auth')
  }

  const HeartIcon = (props) => (
    <Icon {...props} name='log-out-outline'/>
  );

  const [user, setUser] = useState([]);

  useEffect(() => {
    const updateUser = () => {
      getCurrentUserId()
        .then(userId => getUser(userId))
        .then(user => setUser(user))
    }
    updateUser()
  }, [])

  const signOut = () => {
    signUserOut().then(navigateToConnexion)
  }

  return (
    <Layout style={{flex: 1, justifyContent: 'space-around', alignItems: 'center'}}>
      <Card>
        <Text category='h1'>Salut, {user.username} !</Text>
      </Card>
      <Button accessoryRight={HeartIcon} style={{ marginTop: '10%' }} onPress={signOut}>Déconnexion</Button>
    </Layout>
  )
};

export default Profile
import React, {useRef} from 'react';
import {Button, Card, Icon, Input, List, ListItem, Modal, Text} from '@ui-kitten/components';
import {View} from "react-native";
import {getUsers} from "../api";

export const InviteGuests = (props) => {

    const [visible, setVisible] = React.useState(false);
    const [query, setQuery] = React.useState('');
    const [data, setData] = React.useState([]);
    const [guests, setGuests] = React.useState([]);
    const [guestsUpdatedWithProps, setGuestsUpdatedWithProps] = React.useState(false)

    const updateData = () => {
        if (!query) return;

        getUsers("username", encodeURIComponent(query.toLowerCase())).then(results => {
            setData(results)
        })
    };

    React.useEffect(() => {
        setGuestsUpdatedWithProps(true)
        setGuests(props.existingGuests)
    }, [props.existingGuests]);
    React.useEffect(updateData, [query]);
    React.useEffect(() => {
        if (guestsUpdatedWithProps) {
            setGuestsUpdatedWithProps(false)
            return;
        }
        props.onChange(guests)
    }, [guests]);

    const renderInviteAction = (user) => (
        <Button size='small' onPress={() => {
            let guest = {
                userId: user.userId,
                courses: [],
                messages: [],
                status: "Invité",
                username: user.username
            }
            setGuests([...guests, guest])
        }}>Inviter</Button>
    );

    const renderPersonIcon = (props) => (
        <Icon {...props} name='person'/>
    );

    const renderAddPersonIcon = (props) => (
        <Icon {...props} name='person-add-outline'/>
    );

    const renderUser = ({item, index}) => (
        <ListItem
            title={item.username}
            accessoryLeft={renderPersonIcon}
            accessoryRight={() => renderInviteAction(item)}
        />
    );

    return (
        <View>
            <Button size='small' onPress={() => setVisible(true)} accessoryLeft={renderAddPersonIcon} />

            <Modal
                visible={visible}
                backdropStyle={{backgroundColor: 'rgba(0, 0, 0, 0.5)'}}
                onBackdropPress={() => setVisible(false)}
            >
                <Card disabled={true}>
                    <Text category='h1'>Trouver des invités</Text>
                    <Input
                        placeholder='Chercher un invité...'
                        value={query}
                        onChangeText={nextValue => setQuery(nextValue)}
                    />
                    <List style={{ maxHeight: 300, width: '100%' }} data={data} renderItem={renderUser}/>
                    <Button onPress={() => setVisible(false)}>
                        Fermer
                    </Button>
                </Card>
            </Modal>
        </View>
    );
};

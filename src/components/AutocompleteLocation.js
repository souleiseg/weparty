import React from 'react';
import { Autocomplete, AutocompleteItem } from '@ui-kitten/components';

export const AutocompleteLocation = (props) => {

    const [query, setQuery] = React.useState(null);
    const [data, setData] = React.useState([]);

    const updateData = () => {
        if (query == null || query.trim() === '') return;

        // Attention: ne fonctionne qu'avec des adresses en france
        fetch('https://api-adresse.data.gouv.fr/search/?limit=5&q=' + encodeURIComponent(query.toLowerCase()))
            .then(response => response.json())
            .then(json => json.features)
            .then(places => {
                let formattedPlaces = []
                for (const place of places) {
                    formattedPlaces.push({
                        coordinates: {
                            _longitude: place.geometry.coordinates[0],
                            _latitude: place.geometry.coordinates[1],
                        },
                        address: place.properties.label
                    })
                }
                setData(formattedPlaces)
            });
    };

    React.useEffect(() => setQuery(props.defaultValue), [props.defaultValue]);
    React.useEffect(updateData, [query]);

    const onSelect = (index) => {
        setQuery(data[index].address);
        props.onSelect(data[index]);
    };

    const onChangeText = (nextQuery) => {
        setQuery(nextQuery);
    };

    const renderOption = (item, index) => (
        <AutocompleteItem
            key={index}
            title={item.address}
        />
    );

    return (
        <Autocomplete
            label={props.label}
            style={{width: '100%'}}
            value={query}
            onChangeText={onChangeText}
            onSelect={onSelect}>
            {data.map(renderOption)}
        </Autocomplete>
    );
};
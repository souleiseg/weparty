import React from "react";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {BottomNavigation, BottomNavigationTab, Icon} from "@ui-kitten/components";
import {DarkTheme, NavigationContainer} from "@react-navigation/native";
import { createStackNavigator } from '@react-navigation/stack';
import Home from "../screens/Home";
import PartyForm from "../screens/PartyForm";
import Connexion from "../screens/Connexion";
import Inscription from "../screens/Inscription";
import Profile from "../screens/Profile";
import PartyDetails from "../screens/PartyDetails";

const BottomTab = createBottomTabNavigator();
const Stack = createStackNavigator();

const customTheme = {
    ...DarkTheme,
    colors: {
        ...DarkTheme.colors,
        card: '#1A2138'
    }
};

const HomeIcon = (props) => (
    <Icon {...props} name='home-outline'/>
);

const PlusCircleIcon = (props) => (
    <Icon {...props} name='plus-circle-outline'/>
);

const BottomTabBar = ({navigation, state}) => (
    <BottomNavigation selectedIndex={state.index} onSelect={index => navigation.navigate(state.routeNames[index], {})}>
        <BottomNavigationTab title='Accueil' icon={HomeIcon} />
        <BottomNavigationTab title='Nouvelle soirée' icon={PlusCircleIcon} />
        <BottomNavigationTab title='Profil' icon={PlusCircleIcon} />
    </BottomNavigation>
)

const AppNavigator = () => (
    <NavigationContainer theme={customTheme}>
        <Stack.Navigator screenOptions={{headerShown: false}}>
          <Stack.Screen name='Auth' component={AuthNavigator}/>
          <Stack.Screen name='Home' component={HomeNavigator}/>
        </Stack.Navigator>
    </NavigationContainer>
);

const HomeNavigator = () => (
    <BottomTab.Navigator tabBar={props => <BottomTabBar {...props} />} >
      <BottomTab.Screen name='Accueil' component={Home} options={{ tabBarStyle: { display: "none" } }}/>
      <BottomTab.Screen name='Nouvelle soirée' component={PartyForm} options={{tabBarHideOnKeyboard: true}}/>
      <BottomTab.Screen name='Profil' component={Profile}/>
      <BottomTab.Screen name='Voir une soirée' component={PartyDetails}/>
    </BottomTab.Navigator>
);

const AuthNavigator = () => (
    <Stack.Navigator screenOptions={{headerLeft: null}}>
        <Stack.Screen name='Connexion' component={Connexion}/>
        <Stack.Screen name='Inscription' component={Inscription}/>
    </Stack.Navigator>
);

export default AppNavigator
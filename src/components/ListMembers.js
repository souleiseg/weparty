import React, { useState } from 'react';
import {Button, Card, Icon, Input, List, ListItem, Divider, Modal, Text} from '@ui-kitten/components';
import {View} from "react-native";

export const ListMembers = ({onChildPress, data}) => {

    const renderPersonIcon = (props) => (
        <Icon {...props} name='person'/>
    );

    const renderMembers = ({ item, index }) => (
        <ListItem
            accessoryLeft={renderPersonIcon}
            title={`${item.username}`}
            description={`${item.status}`}
        />
    );

    return (
        <View>
            <Card disabled={true} style={{ flexDirection: "column", justifyContent: "center", height: '100%' }}>
                <List
                    data={data}
                    ItemSeparatorComponent={Divider}
                    renderItem={renderMembers}
                />
                <Button size='medium' onPress={onChildPress}>
                    Fermer
                </Button>
            </Card>
        </View>
    );
}
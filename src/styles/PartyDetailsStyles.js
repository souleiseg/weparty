import { StyleSheet } from "react-native";

const PartyDetailsStyles = StyleSheet.create({
     container: {
        flex: 1,
        flexDirection: 'column',
        margin: '5%',
        padding: '5%',
        borderRadius: 12,
        maxHeight: '52%',
    },
    containerMedium: {
        flexDirection: 'column',
        margin: '5%',
        padding: '5%',
        borderRadius: 12,
    },
    containerBottom: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        margin: '5%',
        padding: '5%',
        borderRadius: 12,
    },
    wrapper: {
        marginTop: '5%',
        flexDirection: 'row',
        alignItems: 'center',
        padding: '2%',
    },
    wrapperBottom: {
       justifyContent: 'space-between'
    },
    icon: {
        width: 32,
        height: 32
    },
    isVisible: {
        display: "flex"
    },
    isHidden: {
        display: "none"
    },
    ModalContent: {
        width: '95%', 
        height: 'auto', 
        maxHeight: '80%', 
        justifyContent: 'center'
    },
    ModalCard: {
        flexDirection: "column", 
        paddingVertical: '5%', 
        justifyContent: "center", 
        height: '100%'
    },
    ModalList: {
        paddingVertical: '3%'
    },
    ModalButton: {
        marginTop: '5%'
    }
});

export default PartyDetailsStyles;
import { StyleSheet } from "react-native";

const loginStyles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    layout: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        height: '90%',
        alignSelf: 'flex-end',
    },
    formulaire: {
        paddingVertical: '5%',
        width: '100%',
        paddingHorizontal: '5%',
    },
    bottomLayout: {
        width: '100%',
        height: '33%',
        padding: '5%',
    },
    input: {
        marginBottom: "5%"
    },
    captionContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    captionIcon: {
        width: 10,
        height: 10,
        marginRight: 5
    },
    captionText: {
        fontSize: 12,
        fontWeight: "400",
        color: "#8F9BB3",
    },
    button: {
        marginTop: '5%',
        width: '100%',
    }
});

export default loginStyles;
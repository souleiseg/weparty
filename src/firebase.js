import { initializeApp, getApps } from "@firebase/app";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
  getAuth,
  deleteUser
} from "firebase/auth";
import {Alert, AsyncStorage} from "react-native";
import {getUser, getUsers, postUser} from "./api";

const firebaseConfig = {
  apiKey: "AIzaSyAmWOn49T_kh7iVcBmOJ_Q8DKfYme6JiWw",
  authDomain: "weparty-2f373.firebaseapp.com",
  projectId: "weparty-2f373",
  storageBucket: "weparty-2f373.appspot.com",
  messagingSenderId: "748872030895",
  appId: "1:748872030895:web:0ef910388df73a0779125a"
};

if (getApps().length === 0) {
  initializeApp(firebaseConfig);
}


export const signUserUp = async (email, username, password) =>
  createUserWithEmailAndPassword(getAuth(), email, password)
    .then(credentials => {
      const userId = credentials.user.uid
      saveCurrentUserId(userId)
      try {
        postUser(userId, {
          userId: userId,
          username: username,
          email: email
        })
      } catch (error) {
        deleteUser(credentials.user)
        Alert.alert(
          "L'inscription a échoué",
          "Il s'agit d'un problème avec la base de données, veuillez réessayer plus tard"
        )
        return false;
      }

      return userId
    })
    .catch(error => {
      Alert.alert(
        "L'inscription a échoué",
        error.message
      )
      return false;
    }
  );

export const signUserIn = async (username, password) => {
  const email = await getCorrespondingEmail(username);
  if (!email) {
    Alert.alert("Authentification échouée", "Veuillez saisir un nom d'utilisateur valide");
    return Promise.reject();
  }
  return signInWithEmailAndPassword(getAuth(), email, password)
    .then(credentials => {
      saveCurrentUserId(credentials.user.uid)
      return getUser(credentials.user.uid);
    }).catch(error =>
    Alert.alert(
      "Authentification échouée",
      error.message
    )
  );
}

const getCorrespondingEmail = async (username) => {
  const users = await getUsers("username", username)
  if (!users || users.length === 0) return undefined;
  const email = users[0].email
  return email && email.split('@')[0] === username ? email : undefined
}

export const signUserOut = async () => {
  signOut(getAuth());
  logoutCurrentUserId();
}

const UserIdKey = "USERID_KEY";

const logoutCurrentUserId = async () => {
  AsyncStorage.removeItem(UserIdKey)
}

export const getCurrentUserId = async () => {
  return AsyncStorage.getItem(UserIdKey)
}

const saveCurrentUserId = async (uid) => {
  AsyncStorage.setItem(UserIdKey, uid)
}

export const redirectToHomeIfAlreadyAuthenticated = async (navigation) => {
  const userId = await getCurrentUserId();
  if (userId) {
    navigation.navigate('Home')
  }
}